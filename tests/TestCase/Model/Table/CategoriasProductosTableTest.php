<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoriasProductosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoriasProductosTable Test Case
 */
class CategoriasProductosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoriasProductosTable
     */
    public $CategoriasProductos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.categorias_productos',
        'app.productos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CategoriasProductos') ? [] : ['className' => CategoriasProductosTable::class];
        $this->CategoriasProductos = TableRegistry::getTableLocator()->get('CategoriasProductos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CategoriasProductos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
