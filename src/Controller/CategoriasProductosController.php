<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CategoriasProductos Controller
 *
 * @property \App\Model\Table\CategoriasProductosTable $CategoriasProductos
 *
 * @method \App\Model\Entity\CategoriasProducto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriasProductosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $categoriasProductos = $this->paginate($this->CategoriasProductos);

        $this->set(compact('categoriasProductos'));
    }

    /**
     * View method
     *
     * @param string|null $id Categorias Producto id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoriasProducto = $this->CategoriasProductos->get($id, [
            'contain' => ['Productos']
        ]);

        $this->set('categoriasProducto', $categoriasProducto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $categoriasProducto = $this->CategoriasProductos->newEntity();
        if ($this->request->is('post')) {
            $categoriasProducto = $this->CategoriasProductos->patchEntity($categoriasProducto, $this->request->getData());
            if ($this->CategoriasProductos->save($categoriasProducto)) {
                $this->Flash->success(__('The categorias producto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categorias producto could not be saved. Please, try again.'));
        }
        $this->set(compact('categoriasProducto'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Categorias Producto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoriasProducto = $this->CategoriasProductos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoriasProducto = $this->CategoriasProductos->patchEntity($categoriasProducto, $this->request->getData());
            if ($this->CategoriasProductos->save($categoriasProducto)) {
                $this->Flash->success(__('The categorias producto has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categorias producto could not be saved. Please, try again.'));
        }
        $this->set(compact('categoriasProducto'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Categorias Producto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoriasProducto = $this->CategoriasProductos->get($id);
        if ($this->CategoriasProductos->delete($categoriasProducto)) {
            $this->Flash->success(__('The categorias producto has been deleted.'));
        } else {
            $this->Flash->error(__('The categorias producto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
