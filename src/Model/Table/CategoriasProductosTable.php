<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CategoriasProductos Model
 *
 * @property \App\Model\Table\ProductosTable|\Cake\ORM\Association\HasMany $Productos
 *
 * @method \App\Model\Entity\CategoriasProducto get($primaryKey, $options = [])
 * @method \App\Model\Entity\CategoriasProducto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CategoriasProducto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CategoriasProducto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriasProducto|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CategoriasProducto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriasProducto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CategoriasProducto findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CategoriasProductosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categorias_productos');
        $this->setDisplayField('nombre_categoria');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Productos', [
            'foreignKey' => 'categorias_producto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nombre_categoria')
            ->maxLength('nombre_categoria', 220)
            ->requirePresence('nombre_categoria', 'create')
            ->notEmpty('nombre_categoria');

        return $validator;
    }
}
