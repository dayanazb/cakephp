<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CategoriasProducto $categoriasProducto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Categorias Productos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Productos'), ['controller' => 'Productos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Producto'), ['controller' => 'Productos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categoriasProductos form large-9 medium-8 columns content">
    <?= $this->Form->create($categoriasProducto) ?>
    <fieldset>
        <legend><?= __('Add Categorias Producto') ?></legend>
        <?php
            echo $this->Form->control('nombre_categoria');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
